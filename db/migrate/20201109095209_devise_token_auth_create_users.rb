class DeviseTokenAuthCreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table(:users) do |t|
      ## Required
      t.string :provider, null: false, default: 'email'
      t.string :uid, null: false, default: ''

      ## Database authenticatable
      t.string :encrypted_password, null: false, default: ''

      ## User Info
      t.string :name
      t.string :email, null: false
      t.string :type, null: false, default: 'Merchant'
      t.text :description
      t.integer :status, null: false, default: 1
      t.decimal :total_transaction_sum, precision: 12, scale: 2, null: false, default: 0

      ## Tokens
      t.json :tokens

      t.timestamps
    end

    add_index :users, :email, unique: true
    add_index :users, %i[uid provider], unique: true
    add_index :users, :type
  end
end
