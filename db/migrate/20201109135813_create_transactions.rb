class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.string :uuid, null: false
      t.integer :status, null: false
      t.string :type, null: false
      t.string :customer_email
      t.string :customer_phone
      t.decimal :amount, precision: 12, scale: 2
      t.references :referenced, foreign_key: { to_table: :transactions }

      t.timestamps
    end

    add_index :transactions, :uuid, unique: true
    add_index :transactions, :customer_email
    add_index :transactions, :type
  end
end
