class CreateMerchantTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :merchant_transactions do |t|
      t.references :user, null: false, foreign_key: true
      t.references :transactionable, polymorphic: true, null: false, index: { name: 'index_mrchnt_trns_on_trn_type_and_trn_id' }

      t.timestamps
    end
  end
end
