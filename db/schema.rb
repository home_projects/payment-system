# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_11_10_094433) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "merchant_transactions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "transactionable_type", null: false
    t.bigint "transactionable_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["transactionable_type", "transactionable_id"], name: "index_mrchnt_trns_on_trn_type_and_trn_id"
    t.index ["user_id"], name: "index_merchant_transactions_on_user_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.string "uuid", null: false
    t.integer "status", null: false
    t.string "type", null: false
    t.string "customer_email"
    t.string "customer_phone"
    t.decimal "amount", precision: 12, scale: 2
    t.bigint "referenced_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["customer_email"], name: "index_transactions_on_customer_email"
    t.index ["referenced_id"], name: "index_transactions_on_referenced_id"
    t.index ["type"], name: "index_transactions_on_type"
    t.index ["uuid"], name: "index_transactions_on_uuid", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "name"
    t.string "email", null: false
    t.string "type", default: "Merchant", null: false
    t.text "description"
    t.integer "status", default: 1, null: false
    t.decimal "total_transaction_sum", precision: 12, scale: 2, default: "0.0", null: false
    t.json "tokens"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["type"], name: "index_users_on_type"
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  add_foreign_key "merchant_transactions", "users"
  add_foreign_key "transactions", "transactions", column: "referenced_id"
end
