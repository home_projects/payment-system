# README

* Ruby version 2.7.0

* System dependencies
    * postgres
    * redis
  
##### Docker setup

* Docker Configuration
    * docker-compose pull
    * docker-compose build
    * docker-compose up -d

* Docker Database creation
    * docker-compose exec web bundle exec rake db:create
    * docker-compose exec web bundle exec rake db:migrate
    * docker-compose exec web bundle exec rake users:import
    * docker-compose exec web bundle exec rake transactions:generate

* Docker tests
    * Capybara tests DONT WORK IN DOCKER for now, working on it...
    * docker-compose exec -e RAILS_ENV=test web bundle exec rspec 
  
* Docker .env
    * `PG_USERNAME=postgres`
    * `PG_PASSWORD=password`

* Services (job queues, cache servers, search engines, etc.)
    * DeleteTransactionsWorker - deletes Transactions once per hour

* Admin creds (imported from CSV):
    * admin@mail.com
    * 00000000

##### Or you can set up it locally and run full tests suite including Capybara
* Database creation
    * bundle exec rake db:create
    * bundle exec rake db:migrate
    * bundle exec rake users:import
    * bundle exec rake transactions:generate

* Tests
    * RAILS_ENV=test bundle exec rspec 
