FROM ruby:2.7.0
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client

RUN mkdir /payment-system
WORKDIR /payment-system

COPY Gemfile /payment-system/Gemfile
COPY Gemfile.lock /payment-system/Gemfile.lock

RUN bundle install
ADD . /payment-system

EXPOSE 3000
ENV MALLOC_ARENA_MAX=2

CMD ["bundle", "exec", "puma"]
