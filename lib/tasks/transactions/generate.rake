# frozen_string_literal: true

namespace :transactions do
  # Usage: RAILS_ENV=development bundle exec rake transactions:generate

  desc 'Generates Transactions'
  task generate: :environment do
    merchant = Merchant.first
    %i[transaction_refund transaction_reversal].each do |t_type|
      t = FactoryBot.build(t_type, merchant: merchant)
      t.save!
    end
  end
end
