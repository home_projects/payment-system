# frozen_string_literal: true

require 'csv'

namespace :users do
  # Usage: RAILS_ENV=development bundle exec rake users:import[path_to_csv]

  desc 'Imports Admins and Merchants from a CSV file'
  task :import, %i[path_to_csv] => :environment do |_t, args|
    path_to_csv = args[:path_to_csv] || "#{Rails.root}/db/data/users.csv"
    CSV.foreach(path_to_csv, headers: true) do |row|
      next if User.find_by(email: row.to_h['email'])

      User.create!(row.to_h)
    end
  end
end
