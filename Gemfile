source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.6'

gem 'rails', '~> 6.0.3', '>= 6.0.3.4'
gem 'pg', '~> 1.2', '>= 1.2.3'
gem 'puma', '~> 4.1'

gem 'sass-rails', '>= 6'
gem 'bootstrap', '~> 4.5.2'
gem 'jquery-rails', '~> 4.4'
gem 'sprockets', '~> 3.0'
gem 'slim-rails', '~> 3.2'
gem 'simple_form', '~> 5.0', '>= 5.0.3'
gem 'jbuilder', '~> 2.7'

gem 'redis', '~> 4.0'
gem 'sidekiq', '~> 6.1', '>= 6.1.2'
gem 'sidekiq-cron', '~> 1.2'

gem 'rubocop', '~> 1.2', require: false
gem 'dotenv-rails', '~> 2.7', '>= 2.7.6'
gem 'bootsnap', '>= 1.4.2', require: false

gem 'devise_token_auth', '~> 1.1', '>= 1.1.4'
gem 'email_validator', '~> 2.0', '>= 2.0.1'
gem 'rack-cors', require: 'rack/cors'
gem 'with_advisory_lock', '~> 4.6'
gem 'cancancan', '~> 3.1'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'pry-byebug', '~> 3.9'
  gem 'pry-rails', '~> 0.3.9'
  gem 'awesome_print', '~> 2.0.0.pre'
  gem 'rspec-rails', '~> 4.0', '>= 4.0.1'
  gem 'factory_bot_rails', '~> 6.1'
  gem 'ffaker', '~> 2.17'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '~> 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  gem 'webdrivers'
end
