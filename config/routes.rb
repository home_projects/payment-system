Rails.application.routes.draw do
  devise_for :admins

  root 'manage/transactions#index'

  namespace :manage do
    devise_scope :admin do
      resources :transactions, only: [:index, :show]
      resources :merchants
    end
  end

  namespace :api, defaults: { format: 'json' } do
    mount_devise_token_auth_for 'User', at: 'auth'
    resources :transactions, only: [:create]
  end
end
