# frozen_string_literal: true

class DeleteTransactionsWorker
  include Sidekiq::Worker
  sidekiq_options queue: :low, retry: false

  # NOTE:
  # it might be required to NOT delete All transactions
  # since the Merchant balance depend on transactions history
  # it might be better to delete only transactions which don't affect balance anymore
  # ie: Refund chain and Reversal chain and also with :error status
  def perform
    [
      Transaction::Reversal,
      Transaction::Refund,
      Transaction::Base.error,
      Transaction::Base # if this one is excluded from that list we can keep pair Authorize -> Charge to verify the Merchant Balance
    ].each do |klass_or_relation|
      klass_or_relation.select(:id).find_in_batches(batch_size: 100) do |ids|
        klass_or_relation.where(id: ids).destroy_all
      end
    end
  end
end
