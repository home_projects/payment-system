# frozen_string_literal: true

module Api
  module Transactions
    module Validators
      class Create < ApplicationService
        include ActiveModel::Model

        attr_accessor :transaction, :merchant, :params

        validate :transaction_status
        validate :referenced_present, if: :referenced_required?
        validate :referenced_owner, if: :referenced_required?
        validate :referenced_status, if: :referenced_required?
        validate :referenced_following, if: :referenced_required?
        validate :validate_transaction

        private

        # Let's assume that the status is only :approved on a creation step
        # since this demo app doesn't have any logic that provides transition from { status: nil } to any other status
        #   - all status transitions inside the business logic start from :approved
        #   - we dont have update action in a controller
        def transaction_status
          msg = 'Transaction status is expected to be "approved" on a creation step'
          errors.add(:base, msg) unless transaction.status.to_s == 'approved'
        end

        def referenced_present
          errors.add(:base, 'This type of transaction requires the referenced transaction to be present') unless transaction.referenced
          throw(:abort) unless transaction.referenced
        end

        # gem CanCacCan also helps to describe access rules
        # but lets just use the custom check here
        def referenced_owner
          errors.add(:base, 'The referenced transaction does not belong to the current merchant') if transaction.referenced.merchant.id != merchant.id
        end

        # It works together with #referenced_following
        # Example:
        # we can't Charge if:
        #   - the previous Authorized transaction is not approved
        #   - the previous Authorized transaction has been already reversed
        # But Authorize may have :approved status if it already has the following Charge
        # so another validation #referenced_following helps to prevent such duplications
        # when the same Authorize has many Charges, etc
        def referenced_status
          clear_reference_and_set_error unless transaction.referenced.approved?
        end

        # It helps to prevent duplications and works together with #referenced_status check
        # Ex:
        # if we try to create Charge transaction again - its referenced Authorize transaction already has another following Charge
        # but referenced status is still :approved (no status transition on Charge) so #referenced_status validation will be passed in this case
        # so this validation also checks if the referenced transaction already has following transaction even if the status :approved
        # But also (I suppose) it might be possible that we have Authorize transaction with :error status for some reason
        # in this case it doesnt matter that such transaction doesnt have the following transaction
        # and in this case another validation #referenced_status helps
        # (Note: it might be better also to add uniq index on referenced_id since the referenced can have only one following transaction)
        def referenced_following
          # Dont use transaction.referenced.following which will load current transaction because of "inverse_of" option
          # Dont .find by transaction.referenced_id because it might be set to nil in #clear_reference_and_set_error
          clear_reference_and_set_error if Transaction::Base.find(params[:referenced_id]).following
          # Note:
          # we can't make the transaction invalid here because it will break the logic when we have to create the record with status: :error
          # if the referenced transaction has status != :approved (it is often the case if the referenced transaction already has the following)
        end

        def referenced_required?
          # Here it works by design if the relation "belongs_to :referenced" has been set on a model level
          # (
          #   but it might be set per each STI model as an instance method (that returns true/false)
          #   or it might be the base class instance method that checks
          #   the transaction class name (all names are valid except Authorize)
          # )
          transaction.respond_to?(:referenced)
        end

        def clear_reference_and_set_error
          transaction.referenced_id = nil
          transaction.status = :error
        end

        def validate_transaction
          transaction.validate!
        end
      end
    end
  end
end
