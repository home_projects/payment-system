# frozen_string_literal: true

module Api
  module Transactions
    module Requests
      class Create < ApplicationService
        attr_reader :transaction

        def initialize(params, merchant)
          super()
          @params = params
          @merchant = merchant
          @transaction = Transaction::Base.factory(@params)
        end

        def call
          validate_request!
          save! # it might be preferable to perform it in a background
          self
        end

        private

        def save!
          @merchant.with_advisory_lock("Merchant #{@merchant.id} balance lock", transaction: true) do
            ActiveRecord::Base.transaction do
              @transaction.save!
              @merchant.merchant_transactions.create!(transactionable: @transaction)

              # Her ewe check the Transaction level { status: :error }
              # which means we only can save this transaction and the relation with merchant
              # but we have not to do anything else with the merchant balance or with the referenced transaction status
              # Lets use "return" right here
              # but additional protection on a model level also added (based on status: :error )
              return if @transaction.error?

              @transaction.change_merchant_balance! if @transaction.respond_to?(:change_merchant_balance!)
              @transaction.change_referenced_status! if @transaction.respond_to?(:change_referenced_status!)
            end
          end
        end

        def validate_request!
          Transactions::Validators::Create.new(transaction: @transaction, merchant: @merchant, params: @params).validate!
        end
      end
    end
  end
end
