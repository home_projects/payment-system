# frozen_string_literal: true

module ApplicationHelper
  def active_page_link_class(path:, root: false)
    return unless current_page?(path) || (current_page?('/') && root)

    'active'
  end

  def default_time_format(time)
    return nil unless time.present?

    time = Time.parse(time) if time.is_a? String
    time.strftime('%d.%m.%Y %H:%M %Z')
  end
end
