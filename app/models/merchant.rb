# frozen_string_literal: true

class Merchant < User
  has_many :merchant_transactions, foreign_key: :user_id
  has_many :transactionables, through: :merchant_transactions, source_type: 'Transaction::Base'

  alias transactions transactionables
end
