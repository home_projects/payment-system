# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    can :create, Transaction::Base if user.active?
    can :manage, :all if user.admin_type?
  end
end
