# frozen_string_literal: true

module UuidSupport
  extend ActiveSupport::Concern

  included do
    after_initialize :generate_uuid
  end

  private

  def generate_uuid
    return unless respond_to?(:uuid)

    self.uuid ||= SecureRandom.uuid
  end
end
