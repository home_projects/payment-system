# frozen_string_literal: true

module StiPredicates
  extend ActiveSupport::Concern

  included do
    delegate :type_demodulized_underscored, to: :class
  end

  # It adds a possibility to check STI class type using STI class_name_type?
  # Usage:
  #   ChildClass#authorize_type?
  def method_missing(name, *args)
    if name.to_s.ends_with?('_type?')
      name.to_s.split('_')[0] == type_demodulized_underscored
    else
      super
    end
  end

  def respond_to_missing?(name, include_private = false)
    name.to_s.ends_with?('_type?') || super
  end

  module ClassMethods
    # Transaction::Charge => :charge
    def type_demodulized_underscored
      name.demodulize.underscore
    end

    # charge => Transaction::Charge
    def type_to_class_name(type)
      name.to_s.split('::')[0..-2].push(type.to_s.classify).join('::')
    end
  end
end
