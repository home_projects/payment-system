# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :validatable
  include DeviseTokenAuth::Concerns::User
  include StiPredicates

  enum status: %i[inactive active]

  validates :type, :status, presence: true
end
