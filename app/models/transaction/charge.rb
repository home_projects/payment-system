# frozen_string_literal: true

module Transaction
  class Charge < Base
    has_one :following, class_name: 'Transaction::Refund', foreign_key: 'referenced_id', inverse_of: :referenced
    belongs_to :referenced, class_name: 'Transaction::Authorize', optional: true, inverse_of: :following_charge, dependent: :destroy

    validates :status, inclusion: { in: %w[approved refunded error] }
    validates :amount, numericality: { greater_than: 0 }

    def change_merchant_balance!
      return if error?

      merchant.total_transaction_sum += amount
      merchant.save!
    end
  end
end
