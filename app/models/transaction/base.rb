# frozen_string_literal: true

module Transaction
  class Base < ApplicationRecord
    self.table_name = 'transactions'
    include UuidSupport
    include StiPredicates

    STI_TYPES = %w[
      Transaction::Authorize
      Transaction::Charge
      Transaction::Refund
      Transaction::Reversal
    ].freeze

    enum status: { approved: 0, reversed: 1, refunded: 2, error: 3 }

    has_one :merchant_transaction, as: :transactionable, dependent: :destroy
    has_one :merchant, through: :merchant_transaction

    validates :uuid, :type, :status, presence: true
    validates :customer_email, email: true, allow_nil: true
    # it is supposed that at least one of customer contacts has to be provided (and confirmed during the registration or payment process)
    validates :customer_email, presence: { message: 'field or the phone has to be provided' }, unless: :customer_phone
    validates :customer_phone, presence: { message: 'field or an email has to be provided' }, unless: :customer_email

    scope :of_type, ->(transaction_type) { where(type: "Transaction::#{transaction_type.to_s.classify}") }

    class << self
      def factory(params)
        type = "Transaction::#{params[:type].to_s.classify}"
        new(params.merge(type: type))
      end
    end
  end
end
