# frozen_string_literal: true

module Transaction
  class Authorize < Base
    has_one :following_charge, class_name: 'Transaction::Charge', foreign_key: 'referenced_id', inverse_of: :referenced
    has_one :following_reversal, class_name: 'Transaction::Reversal', foreign_key: 'referenced_id', inverse_of: :referenced

    validates :status, inclusion: { in: %w[approved reversed error] }
    validates :amount, numericality: { greater_than: 0 }

    def following
      following_charge || following_reversal
    end
  end
end
