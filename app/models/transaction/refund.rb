# frozen_string_literal: true

module Transaction
  class Refund < Base
    belongs_to :referenced, class_name: 'Transaction::Charge', optional: true, inverse_of: :following, dependent: :destroy

    validates :status, inclusion: { in: %w[approved error] }
    validates :amount, numericality: { greater_than: 0 }

    def change_merchant_balance!
      return if error?

      merchant.total_transaction_sum -= amount
      merchant.save!
    end

    # would be better to use StateMachine or AASM
    def change_referenced_status!
      return if error?

      referenced&.refunded!
    end
  end
end
