# frozen_string_literal: true

module Transaction
  class Reversal < Base
    belongs_to :referenced, class_name: 'Transaction::Authorize', optional: true, inverse_of: :following_reversal, dependent: :destroy

    validates :status, inclusion: { in: %w[approved error] }
    validates :amount, absence: true

    # would be better to do it using StateMachine or AASM
    def change_referenced_status!
      return if error?

      referenced&.reversed!
    end
  end
end
