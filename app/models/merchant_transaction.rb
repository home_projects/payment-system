# frozen_string_literal: true

class MerchantTransaction < ApplicationRecord
  belongs_to :transactionable, polymorphic: true
  belongs_to :merchant, foreign_key: 'user_id'
end
