// just to redirect on logout
$(() => {
    $('#logout-link-js').on('ajax:success', function() {
        location.href = '/';
    }).on('ajax:error', function() {
        alert('Something went wrong');
    });
});
