$(() => {
    $('.merchant-destroy-link-js').on('ajax:success', function(e, data, status, xhr) {
        location.href = '/manage/merchants';
    }).on('ajax:error', function(e, xhr, status, error) {
        // just to show an error, more perfect tools might be used
        alert(xhr.responseJSON['error']);
    });
});
