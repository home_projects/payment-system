# frozen_string_literal: true

json.extract! transaction, :id, :uuid, :status, :type, :amount, :referenced_id, :customer_email, :customer_phone, :created_at, :updated_at
