# frozen_string_literal: true

json.partial! 'transaction', transaction: @transaction
# NOTE:
# Let's use jbuilder for this demo
# For serious apps it might be slow
# There are plenty of other serializers
# The last one I've worked with is https://github.com/jsonapi-serializer/jsonapi-serializer
