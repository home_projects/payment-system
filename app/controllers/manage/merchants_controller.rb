# frozen_string_literal: true

module Manage
  class MerchantsController < ApplicationController
    before_action :fetch_merchant, only: %i[show edit update destroy]

    def index
      authorize! :read, Merchant
      @merchants = Merchant.all # Note: pagination required in the real app
    end

    def show
      authorize! :read, Merchant
    end

    def new
      @merchant = Merchant.new
    end

    def create
      manage_merchant
    end

    def edit; end

    def update
      manage_merchant
    end

    def destroy
      @merchant.destroy!
      respond_to do |format|
        format.json { head :no_content }
      end
    rescue ActiveRecord::InvalidForeignKey => _e
      respond_to do |format|
        format.json { render json: { error: 'You can not delete a Merchant that has Transactions' }, status: :internal_server_error }
      end
    end

    private

    def manage_merchant
      @merchant = Merchant.find_or_initialize_by(id: params[:id])
      @merchant.assign_attributes(merchant_params)
      if @merchant.save
        redirect_to manage_merchant_path(@merchant)
      else
        render @merchant.persisted? ? :edit : :new
      end
    end

    def fetch_merchant
      @merchant = Merchant.find(params[:id])
    end

    def merchant_params
      params.require(:merchant).permit(:name, :email, :password, :status, :description)
    end
  end
end
