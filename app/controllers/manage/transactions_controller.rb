# frozen_string_literal: true

module Manage
  class TransactionsController < ApplicationController
    before_action :fetch_transaction, only: [:show]

    def index
      authorize! :read, Transaction::Base
      @transactions = Transaction::Base.all # Note: pagination required in the real app
    end

    def show
      authorize! :read, Transaction::Base
    end

    private

    def fetch_transaction
      @transaction = Transaction::Base.find(params[:id])
    end
  end
end
