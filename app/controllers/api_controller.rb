# frozen_string_literal: true

class ApiController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken

  before_action :authenticate_api_user!

  alias current_user current_api_user

  rescue_from StandardError do |e|
    render json: { error: e.message }, status: 500
  end

  rescue_from CanCan::AccessDenied do |e|
    render json: { error: e.message }, status: 403
  end

  rescue_from ActiveRecord::RecordNotFound do |e|
    render json: { error: e.message }, status: 404
  end

  rescue_from ActiveRecord::RecordInvalid,
              ActiveModel::ValidationError,
              ActionController::ParameterMissing do |e|
    render json: { error: e.message }, status: 400
  end
  # Errors types might be extended
end
