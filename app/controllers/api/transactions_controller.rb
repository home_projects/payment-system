# frozen_string_literal: true

module Api
  class TransactionsController < ApiController
    def create
      authorize! :create, Transaction::Base
      @transaction = Transactions::Requests::Create.call(transaction_params, current_user).transaction
    end

    private

    def transaction_params
      params.require(:transaction).permit(:type, :referenced_id, :status, :amount, :customer_email, :customer_phone)
    end
  end
end
