# frozen_string_literal: true

class TransactionPresenter
  def initialize(transaction)
    @transaction = transaction
  end

  def created_at
    @transaction.created_at.strftime('%d.%m.%Y %H:%M %Z')
  end
end
