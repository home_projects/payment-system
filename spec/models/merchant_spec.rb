# frozen_string_literal: true

RSpec.describe Merchant, type: :model do
  let(:merchant_transaction) { create :merchant_transaction }
  let(:merchant) { merchant_transaction.merchant }

  context '#destroy' do
    context 'merchant has transactions' do
      it 'is impossible to delete a merchant if there are related transactions' do
        expect { merchant.destroy }.to raise_error(ActiveRecord::InvalidForeignKey)
        # let's test here that it is not possible to delete it directly from the DB
        expect { merchant.delete }.to raise_error(ActiveRecord::InvalidForeignKey)
      end
    end

    context 'merchant doesn\'t have transactions' do
      before do
        merchant.transactions.destroy_all
      end

      it 'is possible to delete a merchant if there are no related transactions' do
        expect { merchant.destroy }.to change { Merchant.count }.by(-1)
      end
    end
  end
end
