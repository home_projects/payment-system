# frozen_string_literal: true

RSpec.describe Transaction::Reversal, type: :model do
  let(:transaction_authorize) { create :transaction_authorize }
  let(:transaction_charge) { create :transaction_charge }
  let(:transaction_refund) { create :transaction_refund }
  let(:transaction_reversal) { create :transaction_reversal }

  context 'referenced transactions' do
    context 'valid' do
      it 'has the Authorize transaction as its referenced transaction' do
        expect { transaction_reversal.referenced = transaction_authorize }.to_not raise_error
      end
    end

    context 'invalid' do
      let(:transaction_reversal2) { create :transaction_reversal }

      it 'can not have the Refund transaction as its referenced transaction' do
        expect { transaction_reversal.referenced = transaction_refund }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the Charge transaction as its referenced transaction' do
        expect { transaction_reversal.referenced = transaction_charge }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the another Reversal transaction as its referenced transaction' do
        expect { transaction_reversal.referenced = transaction_reversal2 }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end
    end
  end
end
