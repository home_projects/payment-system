# frozen_string_literal: true

RSpec.describe Transaction::Charge, type: :model do
  let(:transaction_authorize) { create :transaction_authorize }
  let(:transaction_charge) { create :transaction_charge }
  let(:transaction_refund) { create :transaction_refund }
  let(:transaction_reversal) { create :transaction_reversal }

  context 'following transactions' do
    context 'valid' do
      it 'has the Refund transaction as its following transaction' do
        expect { transaction_charge.following = transaction_refund }.to_not raise_error
      end
    end

    context 'invalid' do
      let(:transaction_charge2) { create :transaction_charge }

      it 'can not have the Authorize transaction as its following transaction' do
        expect { transaction_charge.following = transaction_authorize }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the Reversal transaction as its following transaction' do
        expect { transaction_charge.following = transaction_reversal }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the another Charge transaction as its following transaction' do
        expect { transaction_charge.following = transaction_charge2 }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end
    end
  end

  context 'referenced transactions' do
    context 'valid' do
      it 'has the Authorize transaction as its referenced transaction' do
        expect { transaction_charge.referenced = transaction_authorize }.to_not raise_error
      end
    end

    context 'invalid' do
      let(:transaction_charge2) { create :transaction_charge }

      it 'can not have the Refund transaction as its referenced transaction' do
        expect { transaction_charge.referenced = transaction_refund }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the Reversal transaction as its referenced transaction' do
        expect { transaction_charge.referenced = transaction_reversal }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the another Charge transaction as its referenced transaction' do
        expect { transaction_charge.referenced = transaction_charge2 }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end
    end
  end
end
