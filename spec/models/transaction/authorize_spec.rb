# frozen_string_literal: true

RSpec.describe Transaction::Authorize, type: :model do
  let(:transaction_authorize) { create :transaction_authorize }
  let(:transaction_charge) { create :transaction_charge }
  let(:transaction_refund) { create :transaction_refund }
  let(:transaction_reversal) { create :transaction_reversal }

  context 'following_charge transaction' do
    context 'valid' do
      it 'has the Charge transaction as its following_charge transaction' do
        expect { transaction_authorize.following_charge = transaction_charge }.to_not raise_error
      end
    end

    context 'invalid' do
      let(:transaction_authorize2) { create :transaction_authorize }

      it 'can not have the Refund transaction as its following_charge transaction' do
        expect { transaction_authorize.following_charge = transaction_refund }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the Reversal transaction as its following_charge transaction' do
        expect { transaction_authorize.following_charge = transaction_reversal }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the another Authorize transaction as its following_charge transaction' do
        expect { transaction_authorize.following_charge = transaction_authorize2 }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end
    end
  end

  context 'following_reversal transaction' do
    context 'valid' do
      it 'has the Reversal transaction as its following_reversal transaction' do
        expect { transaction_authorize.following_reversal = transaction_reversal }.to_not raise_error
      end
    end

    context 'invalid' do
      let(:transaction_authorize2) { create :transaction_authorize }

      it 'can not have the Refund transaction as its following_reversal transaction' do
        expect { transaction_authorize.following_reversal = transaction_refund }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the Charge transaction as its following_reversal transaction' do
        expect { transaction_authorize.following_reversal = transaction_charge }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the another Authorize transaction as its following_reversal transaction' do
        expect { transaction_authorize.following_reversal = transaction_authorize2 }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end
    end
  end
end
