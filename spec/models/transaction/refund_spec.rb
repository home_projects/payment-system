# frozen_string_literal: true

RSpec.describe Transaction::Refund, type: :model do
  let(:transaction_authorize) { create :transaction_authorize }
  let(:transaction_charge) { create :transaction_charge }
  let(:transaction_refund) { create :transaction_refund }
  let(:transaction_reversal) { create :transaction_reversal }

  context 'referenced transactions' do
    context 'valid' do
      it 'has the Charge transaction as its referenced transaction' do
        expect { transaction_refund.referenced = transaction_charge }.to_not raise_error
      end
    end

    context 'invalid' do
      let(:transaction_refund2) { create :transaction_refund }

      it 'can not have the Authorize transaction as its referenced transaction' do
        expect { transaction_refund.referenced = transaction_authorize }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the Reversal transaction as its referenced transaction' do
        expect { transaction_refund.referenced = transaction_reversal }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end

      it 'can not have the another Refund transaction as its referenced transaction' do
        expect { transaction_refund.referenced = transaction_refund2 }.to raise_error(ActiveRecord::AssociationTypeMismatch)
      end
    end
  end
end
