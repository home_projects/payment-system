# frozen_string_literal: true

describe 'Merchants Deletion', type: :feature do
  let!(:admin) { create :admin }
  let!(:merchant) { create :merchant }

  before do
    login_as(admin, scope: :admin)
    visit manage_merchants_path
  end

  context 'without transactions' do
    before { expect(merchant.transactions.count).to be 0 }

    it 'destroys the merchant', js: true do
      expect(page).to have_content 'Merchants List'
      expect(page).to have_selector '.merchant-id-js', text: merchant.id

      click_link 'Destroy'

      expect(page).to have_content 'Merchants List'
      expect(page).to_not have_selector '.merchant-id-js', text: merchant.id
    end
  end

  context 'with transactions' do
    let!(:transaction_refund) { create :transaction_refund, merchant: merchant }  # produces: Authorize -> Charge -> Refund

    before do
      expect(merchant.transactions.count).to be 3
    end

    it 'destroys the merchant', js: true do
      expect(page).to have_content 'Merchants List'
      expect(page).to have_selector '.merchant-id-js', text: merchant.id

      click_link 'Destroy'
      expect(accept_alert).to eq 'You can not delete a Merchant that has Transactions'

      expect(page).to have_content 'Merchants List'
      expect(page).to have_selector '.merchant-id-js', text: merchant.id
    end
  end
end
