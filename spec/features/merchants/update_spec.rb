# frozen_string_literal: true

describe 'Merchant Details', type: :feature do
  let!(:admin) { create :admin }
  let!(:merchant) { create :merchant }

  before do
    login_as(admin, scope: :admin)
    visit edit_manage_merchant_path(merchant)
  end

  context 'valid' do
    let(:merchant_params) do
      {
        name: FFaker::Name.name,
        email: FFaker::Internet.email,
        password: :password,
        status: :inactive,
        description: FFaker::Lorem.sentence
      }
    end

    it 'updates the new merchant', js: true do
      expect do
        within('#merchant-form') do
          fill_in 'Name', with: merchant_params[:name]
          fill_in 'Email', with: merchant_params[:email]
          choose merchant_params[:status]
          fill_in 'Description', with: merchant_params[:description]
          click_button 'Update Merchant'
        end
      end.to change { Merchant.count }.by(0)

      expect(page).to have_content 'Merchant Details'
      expect(page).to have_selector '.merchant-id-js', text: merchant.id

      expect(page).to have_content merchant_params[:name]
      expect(page).to have_content merchant_params[:email]
      expect(page).to have_content merchant_params[:status]
      expect(page).to have_content merchant_params[:description]
    end

    context 'not valid' do
      # just an example of validation
      it 'shows validation errors', js: true do
        expect do
          fill_in 'Email', with: 'wrong'
          click_button 'Update Merchant'
        end.to change { Merchant.count }.by(0)

        expect(page).to have_content 'Edit Merchant'
        expect(page).to have_content 'Please review the problems below'
        expect(page).to have_content 'Email is not an email'
      end
    end
  end
end
