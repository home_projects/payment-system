# frozen_string_literal: true

describe 'Merchant Details', type: :feature do
  let!(:admin) { create :admin }
  let!(:merchant) { create :merchant }

  before do
    login_as(admin, scope: :admin)
    visit manage_merchant_path(merchant.id)
  end

  context 'content' do
    it 'visits the merchant details page', js: true do
      expect(page).to have_content 'Merchant Details'
    end

    it 'shows the merchant table with expected columns', js: true do
      within '.block-merchant' do
        %w[id
           type
           status
           name
           description
           email
           balance].each do |attr|
          th_text = attr.split('_').map(&:camelize).join(' ')
          expect(page).to have_selector 'thead th', text: th_text
        end
      end
    end
  end
end
