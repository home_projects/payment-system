# frozen_string_literal: true

describe 'Merchant Details', type: :feature do
  let!(:admin) { create :admin }

  before do
    login_as(admin, scope: :admin)
    visit new_manage_merchant_path
  end

  context 'valid' do
    # Note:
    # in the real app, if we create the user on our own
    # we would create the user without the 'password' using confirmation/password reset by email
    # (same for the password editing - it might be allowed to edit by admin
    # or if not allowed, so usually only the user itself can do and the confirmation by email is required)
    let(:merchant_params) do
      {
        name: FFaker::Name.name,
        email: FFaker::Internet.email,
        password: :password,
        status: :inactive,
        description: FFaker::Lorem.sentence
      }
    end

    it 'creates the new merchant', js: true do
      expect do
        within('#merchant-form') do
          fill_in 'Name', with: merchant_params[:name]
          fill_in 'Email', with: merchant_params[:email]
          fill_in 'Password', with: merchant_params[:password]
          choose merchant_params[:status]
          fill_in 'Description', with: merchant_params[:description]
          click_button 'Create Merchant'
        end
      end.to change { Merchant.count }.by(1)

      expect(page).to have_content 'Merchant Details'
      expect(page).to have_selector '.merchant-id-js', text: Merchant.last.id

      expect(page).to have_content merchant_params[:name]
      expect(page).to have_content merchant_params[:email]
      expect(page).to have_content merchant_params[:status]
      expect(page).to have_content merchant_params[:description]
    end
  end

  context 'not valid' do
    let(:merchant_params) { {} }

    # just an example of validation
    it 'shows validation errors', js: true do
      expect do
        click_button 'Create Merchant'
      end.to change { Merchant.count }.by(0)

      expect(page).to have_content 'New Merchant'
      expect(page).to have_content 'Please review the problems below'
      expect(page).to have_content "Email can't be blank"
      expect(page).to have_content "Password can't be blank"
    end
  end
end
