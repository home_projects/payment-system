# frozen_string_literal: true

describe 'Merchants List', type: :feature do
  let!(:admin) { create :admin }
  let!(:merchants) { create_list :merchant, 5 }

  before do
    login_as(admin, scope: :admin)
    visit manage_merchants_path
  end

  context 'content' do
    it 'visits an index page', js: true do
      expect(page).to have_content 'Merchants List'
    end

    it 'shows merchants on a page', js: true do
      Merchant.ids.each do |id|
        expect(page).to have_selector '.merchant-id-js', text: id
      end
    end

    it 'shows merchants table with expected columns', js: true do
      %w[id
         type
         status
         name
         description
         email
         balance].each do |attr|
        th_text = attr.split('_').map(&:camelize).join(' ')
        expect(page).to have_selector 'thead th', text: th_text
      end
    end
    # more details checks might be added...
  end
end
