# frozen_string_literal: true

describe 'Transaction Details', type: :feature do
  let!(:admin) { create :admin }
  let!(:transaction) { create :transaction_refund } # produces: Authorize -> Charge -> Refund

  before do
    login_as(admin, scope: :admin)
    visit manage_transaction_path(transaction.id)
  end

  context 'content' do
    it 'visits the transaction details page', js: true do
      expect(page).to have_content 'Transaction Details'
    end

    it 'shows also the transaction merchant details', js: true do
      expect(page).to have_content 'Merchant Details'
    end

    it 'shows transactions table with expected columns', js: true do
      within '.block-transaction' do
        %w[id
           type
           status
           amount
           customer_email
           created_at
           referenced_id].each do |attr|
          th_text = attr.split('_').map(&:camelize).join(' ')
          expect(page).to have_selector 'thead th', text: th_text
        end
      end
    end

    it 'shows the merchant table with expected columns', js: true do
      within '.block-merchant' do
        %w[id
           type
           status
           name
           description
           email
           balance].each do |attr|
          th_text = attr.split('_').map(&:camelize).join(' ')
          expect(page).to have_selector 'thead th', text: th_text
        end
      end
    end
  end
end
