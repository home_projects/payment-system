# frozen_string_literal: true

describe 'Transactions List', type: :feature do
  let!(:admin) { create :admin }
  let!(:transaction) { create :transaction_refund } # produces: Authorize -> Charge -> Refund

  before do
    login_as(admin, scope: :admin)
    visit manage_transactions_path
  end

  context 'content' do
    it 'visits an index page', js: true do
      expect(page).to have_content 'Transactions List'
    end

    it 'shows transactions on a page', js: true do
      Transaction::Base.ids.each do |id|
        expect(page).to have_selector '.transaction-id-js', text: id
      end
    end

    it 'shows transactions table with expected columns', js: true do
      %w[id
         type
         status
         amount
         customer_email
         created_at
         referenced_id].each do |attr|
        th_text = attr.split('_').map(&:camelize).join(' ')
        expect(page).to have_selector 'thead th', text: th_text
      end
    end
    # more details checks might be added...
  end
end
