# frozen_string_literal: true

FactoryBot.define do
  factory :transaction, class: Transaction::Base do
    status { Transaction::Base.statuses[:approved] }
    customer_email { FFaker::Internet.email }
    customer_phone { FFaker::PhoneNumber.phone_number }
    amount { BigDecimal(rand(1..100)) }
    association :merchant

    factory :transaction_authorize, class: Transaction::Authorize

    factory :transaction_charge, class: Transaction::Charge do
      referenced do
        association :transaction_authorize, merchant: merchant
      end
    end

    factory :transaction_refund, class: Transaction::Refund do
      referenced do
        association :transaction_charge, merchant: merchant
      end

      amount { referenced ? referenced.amount : BigDecimal(rand(1..100)) }
    end

    factory :transaction_reversal, class: Transaction::Reversal do
      referenced do
        association :transaction_authorize, merchant: merchant
      end

      amount { nil }
    end
  end
end
