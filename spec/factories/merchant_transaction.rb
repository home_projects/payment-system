# frozen_string_literal: true

FactoryBot.define do
  factory :merchant_transaction do
    association :merchant
    association :transactionable, factory: :transaction_authorize
  end
end
