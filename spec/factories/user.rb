# frozen_string_literal: true

FactoryBot.define do
  factory :user, class: User do
    provider { :email }
    email { FFaker::Internet.email }
    password { :password }
    status { :active }
    name { FFaker::Name.name }
    description { FFaker::Lorem.sentence }

    factory :merchant, class: Merchant

    factory :admin, class: Admin
  end
end
