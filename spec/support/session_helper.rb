# frozen_string_literal: true

# NOTE:
# It is not 100% obvious who is the request owner actually in the real app
# so let's use the merchant role as the role that authorized to perform requests
# (cancancan gem or other tools can help to authorize requests by the role)
# Doing so lets consider the merchant is the transaction owner in this case
shared_context 'current_user' do
  let(:current_user) { create :merchant }

  # Fastest approach to get auth_params which are used as headers to authorize the reqeust
  def auth_params
    current_user.create_new_auth_token
  end

  # "Honest" Approach to retrieve auth params (that shows how login works)
  def login_and_return_auth_params
    post user_session_path, params: { email: current_user.email, password: current_user.password }.to_json,
                            headers: { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }

    get_auth_params_from_login_response_headers(response)
  end

  def get_auth_params_from_login_response_headers(response)
    client = response.headers['client']
    token = response.headers['access-token']
    expiry = response.headers['expiry']
    token_type = response.headers['token-type']
    uid = response.headers['uid']

    {
      'access-token' => token,
      'client' => client,
      'uid' => uid,
      'expiry' => expiry,
      'token-type' => token_type
    }
  end
end
