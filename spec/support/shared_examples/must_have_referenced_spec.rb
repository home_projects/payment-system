# frozen_string_literal: true

shared_examples 'must have referenced' do
  it 'is not possible to create this type of transaction if the reference does not exist' do
    attributes = attributes_for(described_class.name.underscore.gsub('/', '_'))
    instance = described_class.new(attributes)
    errors = instance.tap(&:validate).errors
    expect(errors.messages[:referenced].any? { |m| m == 'must exist' }).to be_truthy
  end
end
