# frozen_string_literal: true

shared_examples 'transaction response' do
  context 'status' do
    it 'responds with success status' do
      expect(response).to have_http_status(200)
    end
  end

  context 'format' do
    it 'has correct structure' do
      attrs = %w[id uuid status type amount referenced_id customer_email customer_phone created_at updated_at]
      expect(json_response.keys.sort).to eq attrs.sort
    end

    it 'has correct type' do
      expect(json_response['type']).to eq Transaction::Base.type_to_class_name(params[:type])
    end

    it 'has correct status' do
      expect(json_response['status'].to_s).to eq params[:status].to_s
    end

    it 'has correct amount' do
      expect(json_response['amount']).to eq params[:amount].round(2).to_s
    end

    it 'has correct referenced_id' do
      expect(json_response['referenced_id']).to eq params[:referenced_id]
    end
  end
end
