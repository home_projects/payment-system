# frozen_string_literal: true

def json_response
  JSON.parse response.body
end

def expect_response_has_error(string)
  expect(json_response['error'].include?(string)).to be_truthy
end
