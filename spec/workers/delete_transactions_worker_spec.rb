# frozen_string_literal: true

describe DeleteTransactionsWorker do
  describe '#perform' do
    # have to be deleted first because these ones have referenced transactions which can't be deleted first
    # (referenced will be deleted automatically using dependent destroy)
    let!(:transactions) { create_list :transaction_refund, 5 }
    let!(:transactions) { create_list :transaction_reversal, 5 }

    # will be deleted separately as transactions with :error status
    let!(:transactions_error) { create_list :transaction_charge, 2, status: :error, referenced_id: nil }

    # other transactions have to be deleted without exceptions since they are not referenced by others
    let!(:transactions_authorize) { create_list :transaction_authorize, 2 }

    it 'enqueues the job' do
      expect { described_class.perform_async }.to change(described_class.jobs, :size).by(1)
    end

    it 'performs the job' do
      subject.perform
      expect(Transaction::Base.count).to eq 0
    end
  end
end
