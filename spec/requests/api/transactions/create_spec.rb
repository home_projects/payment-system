# frozen_string_literal: true

# Please see explanation here: app/services/transactions/validators/create.rb
ALLOWED_STATUSES_ON_CREATE = %i[approved].freeze

TYPE_REFERENCE_MAPPING = {
  charge: :transaction_authorize,
  reversal: :transaction_authorize,
  refund: :transaction_charge
}.freeze

# NOTE:
# Let's use "request" tests in general to validate edge cases using the flow that is close to the real life
# However it might be good to describe the Service tests first with all edge cases
# and here only test that the request works as expected, correct params are passed, etc
# it would speed up tests and we can stub service methods if needed

# STI models have many common test cases but it is likely better to split such tests into files by STI type
RSpec.describe '/transactions', type: :request do
  include_context 'current_user'

  let(:transaction_type) { :authorize }
  let(:transaction_class) { Transaction::Base.factory(type: transaction_type).class }
  let(:referenced_transaction) { nil }

  # For "amount" it maybe will be more correct to set it on a backend side according to a referenced transaction
  # for example for Charge we can set amount of appropriate Authorize transaction
  # and for Refund we can set amount of appropriate Charge
  # but here we trust to what is passed in params and save it
  # (in the real app it might depend on design / flow / conditions)
  let(:amount) { transaction_type.to_s == 'reversal' ? nil : BigDecimal(rand(1..100)) }

  let(:default_params) do
    {
      type: transaction_type,
      referenced_id: referenced_transaction&.id,
      status: :approved,
      customer_email: FFaker::Internet.email,
      customer_phone: FFaker::PhoneNumber.phone_number,
      amount: amount
    }
  end

  describe 'POST /create' do
    subject { post api_transactions_url, params: { transaction: params }, headers: auth_params }

    context 'valid' do
      context 'response' do
        let(:params) { default_params }
        before { subject }
        it_behaves_like 'transaction response'
      end

      context 'Transaction::Authorize' do
        let(:transaction_type) { :authorize }
        let(:params) { default_params }

        it 'creates a new Transaction' do
          expect { subject }.to change(transaction_class.where(status: params[:status]), :count).by(1)
        end

        it 'creates a relation between the Transaction and the Merchant' do
          expect { subject }.to change { current_user.transactions.count }.by(1)
        end

        it 'saves the correct amount' do
          subject
          expect(transaction_class.last.amount).to eq params[:amount].round(2)
        end
      end

      context 'Transaction::Charge' do
        let(:transaction_type) { :charge }
        let!(:referenced_transaction) { create :transaction_authorize, merchant: current_user }
        let(:params) { default_params }

        it 'creates a new Transaction' do
          expect { subject }.to change(transaction_class.where(status: params[:status]), :count).by(1)
        end

        it 'creates a relation between the Transaction and the Merchant' do
          expect { subject }.to change { current_user.transactions.count }.by(1)
        end

        it 'sets the correct referenced Transaction' do
          subject
          expect(transaction_class.last.referenced.id).to eq referenced_transaction.id
        end

        it 'saves the correct amount' do
          subject
          expect(transaction_class.last.amount).to eq params[:amount].round(2)
        end

        it 'increases the Merchant balance' do
          expect { subject }.to change { current_user.reload.total_transaction_sum }.by(params[:amount].round(2))
        end
      end

      context 'Transaction::Refund' do
        let(:transaction_type) { :refund }
        let!(:referenced_transaction) { create :transaction_charge, merchant: current_user }
        let(:params) { default_params }

        it 'creates a new Transaction' do
          expect { subject }.to change(transaction_class.where(status: params[:status]), :count).by(1)
        end

        it 'creates a relation between the Transaction and the Merchant' do
          expect { subject }.to change { current_user.transactions.count }.by(1)
        end

        it 'sets the correct referenced Transaction' do
          subject
          expect(transaction_class.last.referenced.id).to eq referenced_transaction.id
        end

        it 'decreases the Merchant balance' do
          expect { subject }.to change { current_user.reload.total_transaction_sum }.by(-params[:amount].round(2))
        end

        it 'changes referenced status to refunded' do
          subject
          expect(referenced_transaction.reload.status).to eq('refunded')
        end
      end

      context 'Transaction::Reversal' do
        let(:transaction_type) { :reversal }
        let!(:referenced_transaction) { create :transaction_authorize, merchant: current_user }
        let(:params) { default_params.except(:amount) }

        it 'creates a new Transaction' do
          expect { subject }.to change(transaction_class.where(status: params[:status]), :count).by(1)
        end

        it 'creates a relation between the Transaction and the Merchant' do
          expect { subject }.to change { current_user.transactions.count }.by(1)
        end

        it 'sets the correct referenced Transaction' do
          subject
          expect(transaction_class.last.referenced.id).to eq referenced_transaction.id
        end

        it 'changes referenced status to reversed' do
          subject
          expect(referenced_transaction.reload.status).to eq('reversed')
        end
      end
    end

    context 'not valid' do
      context 'response' do
        it 'responds with an error and correct status' do
          post api_transactions_url, params: { transaction: {} }, headers: auth_params
          expect(response).to have_http_status(400)
          expect(json_response.key?('error')).to be_truthy
        end
      end

      context 'user status' do
        before { current_user.inactive! }

        it 'responds with an error if the user status is not active' do
          post api_transactions_url, params: { transaction: {} }, headers: auth_params
          expect(response).to have_http_status(403)
        end
      end

      context 'invalid params' do
        # Let's describe common cases related to invalid params using just one STI type
        context 'common validations' do
          let(:transaction_type) { :authorize }
          let(:params) { default_params }

          it 'does not create a new Transaction if the customer_email has wrong format' do
            expect do
              post api_transactions_url, params: { transaction: params.merge(customer_email: 'wrong') }, headers: auth_params
            end.to change(transaction_class.where(status: params[:status]), :count).by(0)
            expect_response_has_error('email is invalid')
          end

          it 'does not create a new Transaction if the amount is 0' do
            expect do
              post api_transactions_url, params: { transaction: params.merge(amount: 0) }, headers: auth_params
            end.to change(transaction_class.where(status: params[:status]), :count).by(0)
            expect_response_has_error('must be greater than 0')
          end

          it 'does not create a new Transaction if the amount is nil' do
            expect do
              post api_transactions_url, params: { transaction: params.merge(amount: nil) }, headers: auth_params
            end.to change(transaction_class.where(status: params[:status]), :count).by(0)
            expect_response_has_error('is not a number')
          end

          it 'does not create a new Transaction if the type is wrong' do
            expect do
              post api_transactions_url, params: { transaction: params.merge(type: :wrong) }, headers: auth_params
            end.to change(transaction_class.where(status: params[:status]), :count).by(0)
            expect_response_has_error('single-table inheritance mechanism failed to locate the subclass')
          end

          context 'fields absence' do
            it 'does not create a new Transaction if the amount is not passed in params' do
              expect do
                post api_transactions_url, params: { transaction: params.except(:amount) }, headers: auth_params
              end.to change(transaction_class.where(status: params[:status]), :count).by(0)
              expect_response_has_error('is not a number')
            end

            it 'does not create a new Transaction if the status is not passed in params' do
              expect do
                post api_transactions_url, params: { transaction: params.except(:status) }, headers: auth_params
              end.to change(transaction_class.where(status: params[:status]), :count).by(0)
              expect_response_has_error("can't be blank")
            end

            it 'does not create a new Transaction if the type is not passed in params' do
              expect do
                post api_transactions_url, params: { transaction: params.except(:type) }, headers: auth_params
              end.to change(transaction_class.where(status: params[:status]), :count).by(0)
              expect_response_has_error('Invalid single-table inheritance type')
            end

            it 'does not create a new Transaction if both email and phone are absent' do
              expect do
                post api_transactions_url, params: { transaction: params.except(:customer_email, :customer_phone) }, headers: auth_params
              end.to change(transaction_class.where(status: params[:status]), :count).by(0)
              expect_response_has_error('email field or the phone has to be provided')
            end
          end
        end

        context 'status' do
          # Let's test that status limits works the same way for all STI types
          Transaction::Base::STI_TYPES.each do |full_transaction_type|
            context full_transaction_type do
              let(:transaction_class) { full_transaction_type.constantize }
              let(:transaction_type) { transaction_class.type_demodulized_underscored }
              let(:params) { default_params }

              not_allowed_statuses = Transaction::Base.statuses.except(*ALLOWED_STATUSES_ON_CREATE).keys.push(nil)
              not_allowed_statuses.each do |status|
                it "does not create a new Transaction if the status is #{status}" do
                  expect do
                    post api_transactions_url, params: { transaction: params.merge(status: status) }, headers: auth_params
                  end.to change(transaction_class.where(status: params[:status]), :count).by(0)
                end
              end
            end
          end
        end

        context 'amount of Reversal transaction' do
          let(:transaction_type) { :reversal }
          let!(:referenced_transaction) { create :transaction_authorize, merchant: current_user }
          let(:params) { default_params }

          it 'does not create a new Transaction if the amount is set' do
            expect do
              post api_transactions_url, params: { transaction: params.merge(amount: 0) }, headers: auth_params
            end.to change(transaction_class.where(status: params[:status]), :count).by(0)
            expect_response_has_error('Amount must be blank')
          end
        end
      end

      # Let's describe cases related to referenced transaction using just one STI type
      context 'referenced transaction' do
        let(:transaction_type) { :charge }
        let(:params) { default_params }

        context 'without reference' do
          subject { post api_transactions_url, params: { transaction: params.merge(referenced_id: 'wrong') }, headers: auth_params }

          it 'fails if the referenced transaction does not exist' do
            expect { subject }.to change(transaction_class.where(status: params[:status]), :count).by(0)
            expect_response_has_error('requires the referenced transaction to be present')
          end

          it 'does not change the merchant balance' do
            expect { subject }.to change { current_user.reload.total_transaction_sum }.by(0)
          end
        end

        context 'with valid reference but with validation errors' do
          let!(:referenced_transaction) { create :transaction_authorize, merchant: current_user }
          let(:invalid_params) { params.merge(amount: -1000) }

          subject { post api_transactions_url, params: { transaction: invalid_params }, headers: auth_params }

          it 'does not change the merchant balance' do
            expect { subject }.to change { current_user.reload.total_transaction_sum }.by(0)
          end

          it 'does not change the referenced transaction status' do
            expect { subject }.to_not change(referenced_transaction, :status)
          end
        end

        context 'wrong reference type' do
          let!(:referenced_transaction) { create :transaction_charge } # but we are going to create Charge

          it 'fails if the referenced transaction has inappropriate type' do
            expect { subject }.to change(transaction_class.where(status: params[:status]), :count).by(0)
            expect_response_has_error('requires the referenced transaction to be present')
          end

          it 'does not change the merchant balance' do
            expect { subject }.to change { current_user.reload.total_transaction_sum }.by(0)
          end

          it 'does not change the referenced transaction status' do
            expect { subject }.to_not change(referenced_transaction, :status)
          end
        end

        context 'wrong reference status' do
          let!(:transaction_reversal) { create :transaction_reversal, merchant: current_user }
          let!(:referenced_transaction) { transaction_reversal.referenced }

          before do
            transaction_reversal.change_referenced_status!
            # referenced transaction already reversed and has the following transaction
            expect(referenced_transaction.status).to eq 'reversed'
          end

          it 'creates a new Transaction with :error status' do
            expect { subject }.to change(transaction_class.error, :count).by(1)
          end

          it 'creates a new Transaction without reference' do
            expect { subject }.to change(transaction_class.error.where(referenced_id: nil), :count).by(1)
          end

          it 'creates a relation between the new Transaction and the Merchant' do
            expect { subject }.to change { current_user.transactions.error.count }.by(1)
          end

          it 'does not change the merchant balance' do
            expect { subject }.to change { current_user.reload.total_transaction_sum }.by(0)
          end

          it 'does not change the referenced transaction status' do
            expect { subject }.to_not change(referenced_transaction, :status)
          end
        end

        context 'wrong reference owner' do
          let!(:referenced_transaction) { create :transaction_authorize, merchant: random_merchant }
          let!(:random_merchant) { create :merchant }

          it 'creates a new Transaction with :error status' do
            expect { subject }.to change(transaction_class.where(status: params[:status]), :count).by(0)
            expect_response_has_error('The referenced transaction does not belong to the current merchant')
          end

          it 'does not change the merchant balance' do
            expect { subject }.to change { current_user.reload.total_transaction_sum }.by(0)
          end

          it 'does not change the referenced transaction status' do
            expect { subject }.to_not change(referenced_transaction, :status)
          end
        end
      end

      context 'duplicated operations' do
        %w[Transaction::Charge
           Transaction::Refund
           Transaction::Reversal].each do |full_transaction_type|
          context full_transaction_type do
            let(:transaction_class) { full_transaction_type.constantize }
            let(:transaction_type) { transaction_class.type_demodulized_underscored }
            let!(:referenced_transaction) { create TYPE_REFERENCE_MAPPING[transaction_type.to_sym], merchant: current_user }
            let(:params) { default_params }

            it 'creates the only one following Transaction for the referenced one' do
              expect do
                post api_transactions_url, params: { transaction: params }, headers: auth_params
              end.to change(transaction_class.where(status: params[:status]), :count).by(1)
              expect(response).to have_http_status(200)

              expect do
                post api_transactions_url, params: { transaction: params }, headers: auth_params
              end.to change(transaction_class.where(status: params[:status]), :count).by(0)
              expect(response).to have_http_status(200)
            end

            it 'creates the duplicated Transaction with :error status and without the reference' do
              expect do
                post api_transactions_url, params: { transaction: params }, headers: auth_params
              end.to change(transaction_class.where(status: params[:status]), :count).by(1)

              expect do
                post api_transactions_url, params: { transaction: params }, headers: auth_params
              end.to change(transaction_class.where(status: :error, referenced_id: nil), :count).by(1)
              expect(response).to have_http_status(200)
            end
          end
        end

        # Authorize transaction may have the following transaction of two different types (Charge / Reversal)
        # so apart from the duplication testing (when we try to create the transaction of the same type, which already exists)
        # here we test cases when we are going to create the following transaction of the type #1
        # but Authorize transaction already has the following transaction of the type #2
        context 'Authorized transaction already has the following Reversal transaction' do
          # it has not to be possible to create Charge
          let(:transaction_type) { :charge } # we are going to create the Charge for Authorize
          let!(:referenced_transaction) { create :transaction_authorize, merchant: current_user } # Authorize (referenced)
          let!(:transaction_reversal) { create :transaction_reversal, referenced: referenced_transaction }
          let(:params) { default_params }

          before do
            transaction_reversal.change_referenced_status!
          end

          it 'does not create Charge if the referenced transaction has been already reversed' do
            expect { subject }.to change(transaction_class.where(status: params[:status]), :count).by(0)
          end

          it 'creates a new Transaction with :error status' do
            expect { subject }.to change(transaction_class.error, :count).by(1)
          end

          it 'creates a new Transaction without reference' do
            expect { subject }.to change(transaction_class.error.where(referenced_id: nil), :count).by(1)
          end

          it 'creates a relation between the new Transaction and the Merchant' do
            expect { subject }.to change { current_user.transactions.error.count }.by(1)
          end

          it 'does not change the merchant balance' do
            expect { subject }.to change { current_user.reload.total_transaction_sum }.by(0)
          end

          it 'does not change the referenced transaction status' do
            expect { subject }.to_not change(referenced_transaction, :status)
          end
        end

        context 'Authorized transaction already has the following Charge transaction' do
          # it has not to be possible to create Reversal
          let(:transaction_type) { :reversal } # we are going to create the Reverse for Authorize
          let!(:referenced_transaction) { create :transaction_authorize, merchant: current_user } # Authorize (referenced)
          let!(:following_transaction) { create :transaction_charge, referenced: referenced_transaction }
          let(:params) { default_params }

          it 'does not create Reversal if the referenced transaction has been already charged' do
            expect { subject }.to change(transaction_class.where(status: params[:status]), :count).by(0)
          end

          it 'creates a new Transaction with :error status' do
            expect { subject }.to change(transaction_class.error, :count).by(1)
          end

          it 'creates a new Transaction without reference' do
            expect { subject }.to change(transaction_class.error.where(referenced_id: nil), :count).by(1)
          end

          it 'creates a relation between the new Transaction and the Merchant' do
            expect { subject }.to change { current_user.transactions.error.count }.by(1)
          end

          it 'does not change the merchant balance' do
            expect { subject }.to change { current_user.reload.total_transaction_sum }.by(0)
          end

          it 'does not change the referenced transaction status' do
            expect { subject }.to_not change(referenced_transaction, :status)
          end
        end
      end
    end
  end
end
